<?php

class AdminCRMObject extends SqlObject
{
    private $_userTable = "users";
    
    public function getUserByData($login, $hashPassword)
    {
        $sql = "SELECT users.id, users.login, users.password, users.ident_user_type, group_concat(access.ident_access_group separator ',') as groups
                FROM " . $this->_userTable .
                " LEFT JOIN `bluesmoke_access_group2users_types` access " . 
                "ON (users.ident_user_type = access.ident_user_type) " .
                "WHERE login = " . $this->quote($login) .
                "AND password = " . $this->quote($hashPassword);
                
        return $this->query($sql);
    } // end getUserByData
    
    public function getMenusByUserGroups($groups)
    {
        $sql = "SELECT *
                FROM admin_menus
                WHERE ";
                
        foreach ($groups as $group) {
            $sql .= "ident_access_group = " . $this->quote($group);
            $sql .= " OR ";
        }
        
        $sql .= "ident_access_group IS NULL";
        
        return $this->query($sql);
    } // end getMenusByUserGroups
}