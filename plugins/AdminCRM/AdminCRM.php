<?php

class AdminCRM extends Plugin
{
    public function onInit()
    {
        if ($this->controller->user->isSigned()) {
            $this->_setMenus();
        }
        
        parent::onInit();
    }
    
    public function onAccessDenied(Response &$response)
    {
        if (!$this->controller->user->isSigned()) {
            return $this->_onDisplayLogin();
        }
        
        return true;
    } // end onAccessDenied
    
    public function onDistplayStorage(Response &$response, $storageName)
    {
        $storagePlugin = $this->plugins->StoragePlugin;
        $storage = $storagePlugin->createStorageInstanceByXml("$storageName.xml");
        $storage->onAction($response);
        
        return true;
    } // end onDistplayStorage
    
    public function onDefaultDisplay(Response &$response)
    {
        $response->setContent("Hello");
        
        return true;
    } // end onDefaultDisplay
    
    public function onLogin(Response &$response)
    {      
        $login = $_POST['login'];
        $pass = $_POST['password'];
        
        if (!$this->_prepareLoginData($login, $pass)) {
            $errMsg = __("Login details are filled correctly");
            throw new Exception($errMsg . "!");
            return false;
        }
        
        $data = $this->onLoadUserByLoginData($login,$pass);
        
        if (!$data) {
            $errMsg = __("User unknown");
            throw new Exception($errMsg . "!");
            return false;
        }
        
        $this->controller->user->authorization(
            $data['id'],
            $data['login'],
            $data['ident_user_type'],
            $data['groups']
        );
        
        $response->setRedirect("/");
        return true;
    } // end onLogin
    
    public function onLogout(Response &$response)
    {
        $this->controller->user->exitSession();
        
        $response->setRedirect("/");
        return true;
    } // end onLogout
    
    public function onLoadUserByLoginData($login, $hashPassword)
    {
        $data = $this->object->getUserByData($login, $hashPassword);
        
        if (!$data) {
            return false;
        }
        
        $data = $data[0];
        
        if (!$data['id']) {
            return false;
        }
        
        $data['groups'] = explode(' ,', $data['groups']);
        
        return $data;
    } // end onLoadUserByLoginData
    
    private function _onDisplayLogin()
    {
        $guiMaster = $this->controller->masters->GuiMaster;
        $guiMaster->setLayoutPage("login_page.phtml");
        
        return true;
    } // end _onDisplayLogin
    
    private function _setMenus()
    {
        $menus = $this->_getMenus();
        
        $guiMaster = $this->controller->masters->GuiMaster;
        $guiMaster->setProperty("menus", $menus);
        
        return true;
    } // end _setMenus
    
    private function _getMenus()
    {
        $userGroups = $this->controller->user->getOption("groups");
        $menus = $this->object->getMenusByUserGroups($userGroups);
        
        return $menus;        
    } // end getMenus
    
    private function _prepareLoginData(&$login, &$password)
    {
        if (is_null($login) || is_null($password)) {
            return false;
        }
        
        $password = md5($password);
        return true;
    } // end _prepareLoginData
}