<?php

class CreateAction extends AbstractAction
{
    public function action(Response &$response)
    {
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
        
        $createViewContront = $storagePlugin->
            fetchCreateViewByModelAdnData(
                $this->getModel(),
                $this->getData()
        );
            
        $response->setContent($createViewContront);
        return true;
    } // end action
    
    protected function _prepareData()
    {
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
            
        $data = array();
        $data['foreigns'] = $storagePlugin->
            loadForeignKeysByModel($this->getModel());
            
        return $data;
    } // emd _prepareData
}