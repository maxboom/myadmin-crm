<?php

interface IAction
{
    public function action(Response &$response);
}