<?php

class EditAction extends AbstractAction
{
    private $_id;
    
    public function action(Response &$response)
    {
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
            
        $editContent = $storagePlugin->
            fetchEditViewByModelAdnData(
                $this->getModel(),
                $this->getData()
        );
        
        $response->setContent($editContent);
        return true;
    } // end action
    
   
    private function _getID()
    {
        if (!array_key_exists("id", $_REQUEST)) {
            throw new Exception("No Set ID!");
            return false;
        }
        
        return $_REQUEST['id'];
    } // end _getID
    
    private function _prepareIdConditionToModel()
    {
        $model = &$this->getModel();
        
        $coditions = &$model->addChild("conditions");
        
        $idCondition = &$coditions->addChild(
            "condition",
            $this->_id 
        );
        
        $idCondition->addAttribute("column", "id");
        
        return true;
    } // end _prepareIdConditionToModel
    
    protected function _prepareData()
    {
        $this->_id = $this->_getID();
        $this->_prepareIdConditionToModel();
        
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
            
        $data = $storagePlugin->
            loadDataByModel($this->getModel());
            
        if ($data) {
            $data = $data[0];
        }
               
        $foreigns = $storagePlugin->
            loadForeignKeysByModel($this->getModel());
            
        if ($foreigns) {
            $data['foreigns'] = $foreigns;
        }
        
        return $data;
    } // end _prepareData
}