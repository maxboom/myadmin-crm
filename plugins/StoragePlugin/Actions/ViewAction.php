<?php
require_once __DIR__ . "/AbstractAction.php";

class ViewAction extends AbstractAction
{
    private $_page;
    
   
    public function action(Response &$response)
    {
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
            
        $listViewContent = $storagePlugin->
            fetchListViewByModelAndData(
                $this->getModel(),
                $this->getData()
        );
        
        $response->setContent($listViewContent);
        
        return true;
    } // end action
    
    private function _getPage()
    {
        $page = $_REQUEST['page'];
        
        if (!$page) {
            $page = 0;
        }
        
        return $page;
    } // end _getPage
    
    protected function _prepareData()
    {
        $this->_page = $this->_getPage();
        
        $model = $this->getModel();
        $model->addAttribute("page", $this->_page);

        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        $storagePlugin = $pluginsMaster->getPluginInstance("StoragePlugin");
        
        return $storagePlugin->loadDataByModel($this->getModel()); 
    } // end _prepareData
    
    
}