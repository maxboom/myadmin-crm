<?php

class SaveAction extends AbstractAction
{
    public function action(Response &$response)
    {
        $controller = Controller::getInstance();
        $pluginsMaster = $controller->masters->PluginsMaster;
        
        $storagePlugin = $pluginsMaster->
            getPluginInstance("StoragePlugin");
            
            
        $storagePlugin->createOrUpdateData($this->getData());
        
        $response->setRedirect("/" . $_REQUEST['path']);
    } // end action
    
    protected function _prepareData()
    {
        $data = array();
        
        $data = $this->_prepareDataWithUpdate();
        
        if (!$_REQUEST['id']) {
            $data = $this->_prepareDataWithCreate();
        }
                
        $model = &$this->getModel();
        
        $data['table'] = (string)$model['table'];
        $data['values'] = array();
        
        foreach ($model->fields->field as $field) {
            $praparing = $this->_prepareField($field);
            
            if ($praparing === false) {
                continue;
            }
            
            $value = $this->
                _getRequestDataByKey((string)$field['caption']);
            
            $data['values'][(string)$field['column']] = $value;
        }
        
        
        return $data;
    } // end _prepareData
    
    private function _getRequestDataByKey($key)
    {
        $key = str_replace(" ", "_", $key);
        
        if (!array_key_exists($key, $_REQUEST)) {
           $errMsg = __("Data is not Correct! " . $key);
            throw new Exception($errMsg);
        }
        
        return $_REQUEST[$key];
    } // end _getRequestDataByKey
    
    private function _prepareField($field)
    {
        if ((string)$field['only_list'] === "true") {
            return false;
        }
        
        return $this->
            _getRequestDataByKey((string)$field['caption']);
    } // end _prepareField
    
    private function _prepareDataWithCreate()
    {
        $data = array(
            'action' => "INSERT"
        );
        
        return $data;
    } // end _prepareDataWithCreate
    
    private function _prepareDataWithUpdate()
    {
        $data = array(
            'action' => "UPDATE",
            'condition_id' => $_REQUEST['id']
        );
        
        return $data;
    } // end _prepareDataWithUpdate
}