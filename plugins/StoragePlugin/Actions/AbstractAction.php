<?php
require_once __DIR__ . "/IAction.php";

abstract class AbstractAction implements IAction
{
    private $_model;
    
    private $_data;
    
    private $_storage;
    
    public function __construct(Storage &$storage)
    {
        $this->_model = &$storage->getModel();
        $this->_data = $this->_prepareData();
        $this->_storage = &$storage;
    } // end __construct
    
    public function action(Response &$response)
    {
        $errMsg = __("Fucking Method");
        throw new Exception($errMsg);
        return false;
    } // end action
    
    abstract protected function _prepareData();
    
    public function &getData()
    {
        return $this->_data;
    } // end getData
    
    public function &getStorage()
    {
        return $this->_storage;
    } // end getStorage
    
    public function &getModel()
    {
        return $this->_model;
    } // end getModel
}