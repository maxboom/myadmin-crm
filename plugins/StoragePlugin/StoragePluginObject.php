<?php

class StoragePluginObject extends SqlObject
{
    public function getDataByModel($model)
    {
        $table = $model['table'];
        
        $sql = "SELECT $table.id, ";
        
        foreach ($model->fields->field as $field) {
            $caption = $field['caption'];
            $column = $field['column'];
            
            
            if ($field['type'] != "foreign_key") {
                $sql .= " $table.$column AS $caption, ";
                continue;
            }
            
            $foreignTable = $field['foreign_table'];
            $foreignColumn = $field['foreign_column'];
            
            $foreignTable = substr($foreignTable, 0, 1) . 
                "_" . substr($foreignTable, -1);
                
            $caption = str_replace(" ", "_", $caption);
                
            $captionKey = $this->quote($caption . "_key");            
            $caption = $this->quote($caption);
            
                
            $sql .= " $foreignTable.$foreignColumn as $caption, ";
            $sql .= " $table.$column as $captionKey, ";
        }
        
        $sql = substr($sql, 0, -2);
        
        $sql .= " FROM $table ";
        
        foreach ($model->fields->field as $field) {
            if ($field['type'] != "foreign_key") {
                continue;
            }
            
            $table = $model['table'];
            $column = $field['column'];
            
            $foreignTable = $field['foreign_table'];
            $foreignTableUnical = substr($foreignTable, 0, 1) . 
                    "_" . substr($foreignTable, -1);
                    
            $foreingComparsion = $field['foreign_comparison_table'];
            
            $join = "LEFT JOIN";
            
            if ($field['is_null']) {
                $join = "LEFT OUTER JOIN";
            }
            
            $sql .= $join . " $foreignTable $foreignTableUnical ON ($table.$column = " .
                "$foreignTableUnical.$foreingComparsion)"; 
        }
        
        $sql = $this->_setConditionToSqlByModel($sql, $model);
        
        $limit = $model['limit'];
        $limitFrom = $limit * $model['page'];
        
        $sql .= " LIMIT $limitFrom,$limit";
        
               
        return $this->query($sql);
    } // end getDataByModel
    
    public function getForeignKeysByModelField($field)
    {
        $table = $field['foreign_table'];
        $column = $field['foreign_comparison_table'];
        $caption = $field['foreign_column'];
        
        
        $sql = "SELECT $table.$column as id, $table.$caption " .
                "FROM $table";
                
        return $this->query($sql); 
    } // end getForeignKeysByModelField
    
    public function addOrUpdateData($data)
    {
        $sql = "";
        
        if ($data['action'] == "UPDATE") {
            $sql = $this->
                _getUpdateSqlByData($data);
        }
        
        if ($data['action'] == "INSERT") {
            $sql = $this->
                _getInsertSqlByData($data);
        }
        
        return $this->query($sql);
    } // end addOrUpdateDataByModel
    
    private function _getInsertSqlByData($data)
    {
        $table = $data['table'];
        $sql = "INSERT INTO $table ";
        
        $insertValues = "(";
        $values = " VALUES (";
        
        foreach ($data['values'] as $column => $value) {
            $insertValues .= "$column, ";
            $valueStirng = $this->quote($value);
            
            if (!$value) {
                $valueStirng = "NULL";
            }
            
            $values .= $valueStirng . ", ";
        }
        
        $insertValues = substr($insertValues, 0, -2);
        $values = substr($values, 0, -2);
        
        $insertValues .= ")";
        $values .= ")";
        
        $sql .= $insertValues . $values;
        
        return $sql;
    } // end _getInsertSqlByData
    
    private function _getUpdateSqlByData($data)
    {
        $table = $data['table'];
        $sql = "UPDATE $table SET ";
        
        foreach ($data['values'] as $column => $value) {
            $valueString = $this->quote($value);
            
            if (empty($value)) {
                $valueString = "NULL";
            }
            
            $sql .= "$table.$column = $valueString, ";
        }
        
        $conditionID = $data['condition_id'];
        
        $sql = substr($sql, 0, -2);
        $sql .= " WHERE id = " . $this->quote($conditionID);
        
        
        return $sql;
    } // end _getUpdateSqlByData
    
    private function _setConditionToSqlByModel($sql, $model)
    {
        if (!isset($model->conditions->condition)) {
            return $sql;
        }
        
        $sql .= " WHERE ";
        
        $table = $model['table'];
        
        foreach ($model->conditions->condition as $condition) {
            $conditionColumn = $condition['column'];
            $conditionValue = (string)$condition;
            
            $sql .= "$table.$conditionColumn = $conditionValue AND ";
        }
        
        $sql = substr($sql, 0, -5);
        
        return $sql;
    } // end _setConditionToSqlByModel
}