<?php

require_once URL_ROOT . '/plugins/StoragePlugin/Storage.php';

class StoragePlugin extends Plugin
{
    public function createStorageInstanceByJson($jsonString)
    {
        $parsedArray = $this->_getStorageArrayByJson($jsonString);
        return new Storage($parsedArray);
    } // end createStorageInstaceByJson
    
    public function createStorageInstanceByXml($storageFile)
    {
        $storageSting = $this->_getStorageFile($storageFile);
        
        $model = $this->_getStorageArrayByXml($storageSting);
        
        return new Storage($model);
    } // end createStorageInstanceByXml
    
    public function loadDataByModel($model)
    {
        return $this->object->getDataByModel($model);
    } // end loadDataByModel
    
    public function loadForeignKeysByModel($model)
    {
        $foreigns = array();
        
        foreach ($model->fields->field as $field) {
            if ($field['type'] != "foreign_key") {
                continue;
            }
            
            $caption = (string)$field['caption'];
            
            $foreigns[$caption] = $this->object->
                getForeignKeysByModelField($field);            
        }
        
        return $foreigns;
    } // end loadForeignKeysByModel
    
    public function createOrUpdateData($data)
    {
        return $this->object->
            addOrUpdateData($data);
    } // end addOrUpdateDataByModel
    
    public function fetchEditViewByModelAdnData($model, $data)
    {
        $this->model = $model;
        $this->data = $data;
        
        return $this->fetch("edit_view.phtml");
    } // end fetchEditViewByModelAdnData
    
    public function fetchCreateViewByModelAdnData($model, $data)
    {
        $this->model = $model;
        $this->data = $data;
        return $this->fetch("edit_view.phtml");
    } // end fetchCreateViewByModelAdnData
    
    public function fetchListViewByModelAndData($model, $data)
    {
        $this->model = $model;
        $this->data = $data;
        return $this->fetch("list_view.phtml");
    } // end fetchListViewByModelAndData
        
    private function _getStorageArrayByXml($xmlFile)
    {
        return simplexml_load_file($xmlFile);
    } // end _getStorageArrayByXml
    
    private function _getStorageArrayByJson($jsonString)
    {
        return json_decode($jsonString);
    } // end _getStorageArrayByJson
    
    private function _getStorageFile($fileName)
    {
        $storageDirectory =$this-> _getStorageDirectory($fileName);
        
        $storageFile = $storageDirectory . "/storages/" . $fileName;
        
        if (!file_exists($storageFile)) {
            $errMsg = __("File $storageFile not found!");
            throw new Exception($errMsg);
        }
        
        return $storageFile;
    } // end _getStorageFile
    
    private function _getStorageDirectory($fileName)
    {
        $debug = debug_backtrace();
        $lastCaller = $debug[2]; // real las caller in 2 key
        
        $callerFile = $lastCaller['file'];
        
        return dirname($callerFile);
    } // end _getStorageDirectory
    
}