<?php

$this->_properties = array(
    'js'         => array(
        "//code.jquery.com/jquery-2.1.3.js",
        "//code.jquery.com/ui/1.11.4/jquery-ui.js",
        "/core/Masters/GuiMaster/template/scripts/bluesmoke.js", //very need
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
        "//ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js",
        "/themes/admin-crm/static/js/app.min.js",
        "/themes/admin-crm/static/lib_sweetalert/sweet-alert.min.js",
    ),
	'css'        => array(
        "/themes/admin-crm/static/css/AdminLTE.min.css",
        "/themes/admin-crm/static/css/skins/_all-skins.min.css",
        "/themes/admin-crm/static/lib_sweetalert/sweet-alert.css",
        "/themes/admin-crm/static/datepicker/datepicker.css",
        "//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css",
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
        "//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css",
        "//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
        //"//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
    ),
    'lang'       => "en-US",
	'caption'    => 'Admin-CRM',
	'meta'       => array(
        'description' => "admin-crm",
        'author'      => "zmaxboomz@gmail.com",
        'keywords'    => "php, framework, js, web" 
     ),
    'icon'       => '/core/Masters/GuiMaster/template/bluesmoke.ico',
);